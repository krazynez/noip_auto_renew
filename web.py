from selenium.webdriver import Firefox
from selenium.webdriver.firefox.options import Options as FirefoxOptions
from selenium.common.exceptions import ElementClickInterceptedException, NoSuchElementException, WebDriverException
from selenium.webdriver.common.by import By

from commandlineargs import CommandLineArgs
import logging


class Web:
    def __init__(self):
        self.command_line_args = CommandLineArgs().args
        self.username = self.command_line_args.username
        self.password = self.command_line_args.password

        self.driver = self._create_web_driver()

    def _create_web_driver(self) -> Firefox:
        try:
            if not self.command_line_args.debug:
                options = FirefoxOptions()
                options.add_argument('--headless')
                return Firefox(options=options)
            else:
                return Firefox()
        except WebDriverException as e:
            logging.exception(e)
            exit()
        except KeyboardInterrupt as e:
            exit()

    def login(self):
        LOGIN_URL = 'https://www.noip.com/login'
        self.driver.get(LOGIN_URL)
        self.insert_username()
        self.insert_password()
        self.click_login()

    def insert_password(self):
        form_input = self.driver.find_element(By.NAME, 'password')
        form_input.send_keys(self.password)

    def insert_username(self):
        form_input = self.driver.find_element(By.NAME, 'username')
        form_input.send_keys(self.username)

    def click_login(self):
        login_button = self.driver.find_element(By.ID, 'clogs-captcha-button')
        login_button.click()

    def quit(self):
        self.driver.quit()

## Setup

- [Geckodriver](https://github.com/mozilla/geckodriver/releases) is needed in PATH

- `pip3 install -r requirements.txt`

- `chmod +x run.py`

## Running

- `./run.py [-u|--username] <username/email> [-p|--password] <password>`

- Let it do its magic ;-)

## TODO's

- Test when my domain names need renewal

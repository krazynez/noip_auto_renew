#!/usr/bin/env python3
from selenium.common.exceptions import ElementClickInterceptedException, NoSuchElementException, WebDriverException
from selenium.webdriver.common.by import By
from web import Web
from commandlineargs import CommandLineArgs
import time
import platform
import logging
import sys


def nav(driver):
    # Go to active hostname page
    try:
        # Need the sleep or the processing happens before the page loads
        time.sleep(5)
        active_host_btn = driver.find_element(By.CLASS_NAME, "host-data-widget")
        active_host_btn.click()
    except NoSuchElementException as e:
        logging.exception(e)
        logging.info(
            'Your account is probably locked out for 15 Mins: try -d option for GUI to show error')
        logging.info('Did you enter your username/password right?')
        return

    # Needed or won't have enough time to fetch classes
    time.sleep(3)
    # count active hostnames
    total_hostnames = driver.find_elements(By.CLASS_NAME, 'text-right-md')

    for items in total_hostnames:
        try:
            items.find_element(By.CLASS_NAME, 'btn').click()
            # TODO: have this print not show up if Renewal is not needed
            logging.info('Your domain names have been updated.')
        except ElementClickInterceptedException:
            logging.info('Renewal of domain(s) not needed')
            sys.exit(1)


            expire = driver.find_element(By.CLASS_NAME, 'no-link-style')
            domains = driver.find_element(By.CLASS_NAME, 'cursor-pointer')
            domains_dic = {}
            expired_times = {}
            i = 0
            j = 0
            k = 0
            for domain in domains:
                domains_dic[i] = domain.text
                i += 1
            for expire_time in expire:
                expired_times[j] = expire_time.text
                j += 1
            for domain_name in domains_dic:
                logging.info(domains_dic[domain_name], ":", expired_times[k])
                k += 1
            return


def main():
    if os_is_supported():
        web_driver = Web()
        web_driver.login()
        time.sleep(8)
        nav(web_driver.driver)
        web_driver.quit()


def os_is_supported() -> bool:
    logging.info("Checking to see if OS is supported")
    if platform.machine() not in ("AMD64", "x86_64", "arm7l", "aarch64"):
        logging.critical("Platform is currently not supported")
        return False
    return True


if __name__ == '__main__':
    main()

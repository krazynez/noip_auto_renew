import argparse


class CommandLineArgs:
    def __init__(self):
        self.parser = self._create_parser()
        self._add_arguments()
        self.args = self.parser.parse_args()

    def _create_parser(self):
        return argparse.ArgumentParser(description='Used to auto renew noip certs')

    def _add_arguments(self):
        self._username_argument()
        self._password_argument()
        self._debug_argument()

    def _username_argument(self):
        self.parser.add_argument('-u', '--username', action='store',
                                 required=True, help='username or email')

    def _password_argument(self):
        self.parser.add_argument('-p', '--password', action='store',
                                 required=True, help='password')

    def _debug_argument(self):
        self.parser.add_argument('-d', '--debug', action='store_true',
                                 help='run Firefox without being headless')
